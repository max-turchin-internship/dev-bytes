package com.sannedak.devbytes.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sannedak.devbytes.data.repository.DevBytesRepository
import com.sannedak.devbytes.presentation.model.UiDevByteMapper

class DevBytesViewModelFactory(
    private val repository: DevBytesRepository,
    private val mapper: UiDevByteMapper
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DevBytesViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return DevBytesViewModel(repository, mapper) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
