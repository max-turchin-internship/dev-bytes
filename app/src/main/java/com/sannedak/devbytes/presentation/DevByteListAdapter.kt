package com.sannedak.devbytes.presentation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.net.toUri
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.sannedak.devbytes.R
import com.sannedak.devbytes.databinding.ListItemDevByteBinding
import com.sannedak.devbytes.presentation.model.UiDevByte

class DevByteListAdapter(private val redirectToYoutube: (videoUrl: String) -> Unit) :
    ListAdapter<UiDevByte, DevByteItemViewHolder>(DevByteDiffCallBack()) {

    init {
        setHasStableIds(true)
    }

    private val list = mutableListOf<UiDevByte>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DevByteItemViewHolder {
        return DevByteItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.list_item_dev_byte, parent, false
            )
        )
    }

    override fun getItemId(position: Int): Long {
        val positionInRecycler = list[position].id
        return positionInRecycler.toLong()
    }

    override fun onBindViewHolder(holder: DevByteItemViewHolder, position: Int) {
        holder.bind(list[position], redirectToYoutube)
    }

    override fun getItemCount() = list.size

    fun refillList(newList: List<UiDevByte>) {
        list.clear()
        list.addAll(newList)
        submitList(newList)
    }
}

class DevByteItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val binding = ListItemDevByteBinding.bind(view)

    fun bind(listItem: UiDevByte, redirectToYoutube: (videoUrl: String) -> Unit) {
        val imageUrl = listItem.thumbnailUrl.toUri().buildUpon().scheme("https").build()
        Glide.with(binding.root)
            .load(imageUrl)
            .apply(RequestOptions())
            .into(binding.imageVideoThumbnail)

        binding.textDescription.text = listItem.description
        binding.textTitle.text = listItem.title
        binding.cardDevByte.setOnClickListener { redirectToYoutube(listItem.videoUrl) }
    }
}

class DevByteDiffCallBack : DiffUtil.ItemCallback<UiDevByte>() {

    override fun areItemsTheSame(oldItem: UiDevByte, newItem: UiDevByte) = oldItem.id == newItem.id

    override fun areContentsTheSame(oldItem: UiDevByte, newItem: UiDevByte) = oldItem == newItem
}
