package com.sannedak.devbytes.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.sannedak.devbytes.data.DataState
import com.sannedak.devbytes.data.model.DevByteResponse
import com.sannedak.devbytes.data.repository.DevBytesRepository
import com.sannedak.devbytes.presentation.model.UiDevByte
import com.sannedak.devbytes.presentation.model.UiDevByteMapper
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class DevBytesViewModel(
    private val repository: DevBytesRepository,
    private val mapper: UiDevByteMapper
) : ViewModel() {

    private val _uiState = MutableStateFlow<UiState<Any>>(UiState.Idle)

    val uiState: StateFlow<UiState<Any>>
        get() = _uiState

    init {
        loadData()
    }

    private fun loadData() {
        viewModelScope.launch {
            val dataFlow = repository.getDevBytes()
            dataFlow.collect {
                when (it) {
                    is DataState.Downloading -> _uiState.value = UiState.Loading
                    is DataState.Error -> TODO()
                    is DataState.Success -> _uiState.value =
                        UiState.DevBytesOnScreen(mapToUiObject(it.data))
                }
            }
        }
    }

    private fun mapToUiObject(data: List<DevByteResponse>): List<UiDevByte> {
        return mapper.map(data)
    }
}
