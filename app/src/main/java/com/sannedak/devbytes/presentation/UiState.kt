package com.sannedak.devbytes.presentation

import com.sannedak.devbytes.presentation.model.UiDevByte

sealed class UiState<T> {

    object Loading : UiState<Any>()
    object Idle : UiState<Any>()
    data class DevBytesOnScreen<T>(val data: List<UiDevByte>): UiState<T>()
}
