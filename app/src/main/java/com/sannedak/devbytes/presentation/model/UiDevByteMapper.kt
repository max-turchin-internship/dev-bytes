package com.sannedak.devbytes.presentation.model

import com.sannedak.devbytes.data.model.DevByteResponse

class UiDevByteMapper {

    private var id = 0

    fun map(list: List<DevByteResponse>): List<UiDevByte> {
        return list.map {
            UiDevByte(
                mapId(),
                it.title,
                it.description,
                it.videoUrl,
                it.updatedDate,
                it.thumbnailUrl
            )
        }
    }

    private fun mapId() = id++
}