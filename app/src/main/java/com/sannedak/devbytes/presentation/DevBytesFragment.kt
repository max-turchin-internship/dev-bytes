package com.sannedak.devbytes.presentation

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.sannedak.devbytes.DevBytesApplication
import com.sannedak.devbytes.databinding.FragmentDevBytesBinding
import com.sannedak.devbytes.presentation.model.UiDevByte
import kotlinx.coroutines.flow.collect

class DevBytesFragment : Fragment() {

    private var _binding: FragmentDevBytesBinding? = null

    private val listAdapter by lazy { DevByteListAdapter(::redirectToYoutube) }
    private val application by lazy { (requireActivity().application as DevBytesApplication) }
    private val viewModel: DevBytesViewModel by viewModels {
        DevBytesViewModelFactory(application.repository, application.devBytesMapper)
    }

    private val binding
        get() = _binding!!

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDevBytesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecycler()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun observeViewModel() {
        lifecycleScope.launchWhenStarted {
            viewModel.uiState.collect {
                when (it) {
                    UiState.Loading -> showLoading()
                    is UiState.DevBytesOnScreen -> showDevBytes(it.data)
                }
            }
        }
    }

    private fun setUpRecycler() {
        binding.listDevBytes.adapter = listAdapter
    }

    private fun showLoading() {
        binding.listDevBytes.setVisible(false)
        binding.groupLoading.setVisible(true)
    }

    private fun showDevBytes(data: List<UiDevByte>) {
        binding.listDevBytes.setVisible(true)
        binding.groupLoading.setVisible(false)
        listAdapter.refillList(data)
    }

    private fun redirectToYoutube(videoUrl: String) {
        val youtubeIntent = Intent(Intent.ACTION_VIEW, Uri.parse(videoUrl))
        startActivity(youtubeIntent)
    }
}
