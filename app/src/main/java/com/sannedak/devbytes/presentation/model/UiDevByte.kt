package com.sannedak.devbytes.presentation.model

data class UiDevByte(

    val id: Int,
    val title: String,
    val description: String,
    val videoUrl: String,
    val updatedDate: String,
    val thumbnailUrl: String
)