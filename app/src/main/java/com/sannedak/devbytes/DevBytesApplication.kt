package com.sannedak.devbytes

import android.app.Application
import android.content.res.Resources
import androidx.work.*
import com.sannedak.devbytes.data.database.DevBytesDatabase
import com.sannedak.devbytes.data.network.RetrofitFactory
import com.sannedak.devbytes.data.repository.DevBytesMockRepository
import com.sannedak.devbytes.data.repository.DevBytesRepositoryImpl
import com.sannedak.devbytes.data.repository.DevBytesUdacityRepository
import com.sannedak.devbytes.presentation.model.UiDevByteMapper
import com.sannedak.devbytes.worker.RefreshDataWorker
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit


class DevBytesApplication : Application() {

    companion object {
        private lateinit var appResources: Resources
    }

    private val database by lazy { DevBytesDatabase.getDatabase(this) }
    private val retrofit by lazy { RetrofitFactory.create() }
    private val udacityService by lazy { retrofit.create(DevBytesUdacityRepository::class.java) }
    private val mockService by lazy { DevBytesMockRepository() }

    private val _repository by lazy {
        DevBytesRepositoryImpl(
            udacityService,
            database.devBytesDao(),
            mockService
        )
    }
    private val _devBytesMapper by lazy { UiDevByteMapper() }

    val repository
        get() = _repository

    val devBytesMapper
        get() = _devBytesMapper

    override fun onCreate() {
        super.onCreate()
        appResources = resources
        delayedInit()
    }

    private fun delayedInit() {
        CoroutineScope(Dispatchers.Default).launch {
            setupRecurringWork()
        }
    }

    private fun setupRecurringWork() {
        val constraints = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.UNMETERED)
            .setRequiresBatteryNotLow(true)
            .setRequiresCharging(true)
            .apply {
                setRequiresDeviceIdle(true)
            }.build()

        val repeatingRequest =
            PeriodicWorkRequestBuilder<RefreshDataWorker>(1, TimeUnit.DAYS)
                .setConstraints(constraints)
                .build()

        WorkManager.getInstance(applicationContext).enqueueUniquePeriodicWork(
            RefreshDataWorker.WORK_NAME,
            ExistingPeriodicWorkPolicy.KEEP,
            repeatingRequest
        )
    }
}
