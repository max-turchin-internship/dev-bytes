package com.sannedak.devbytes.data.repository

import android.util.Log
import com.sannedak.devbytes.data.DataState
import com.sannedak.devbytes.data.model.DevByteEntity
import com.sannedak.devbytes.data.model.DevByteResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.withContext

class DevBytesRepositoryImpl(
    private val udacityService: DevBytesUdacityRepository,
    private val databaseDao: DevBytesDao,
    private val mockService: DevBytesMockRepository
) : DevBytesRepository {

    override suspend fun getDevBytes(): Flow<DataState<Any>> {
        return getFromNetwork()
    }

    suspend fun saveDevBytesToDatabase() {
        withContext(Dispatchers.IO) {
            val response = udacityService.getDevBytes()
            val mappedResponse = mapToDatabaseObject(response)
            databaseDao.insertAll(mappedResponse)
            Log.i("DATABASE", "SAVEDTODATABASE")
        }
    }

    private fun getFromNetwork() = flow {
        emit(DataState.Downloading)
        val response = udacityService.getDevBytes()
        //saveDevBytesToDatabase()
        emit(DataState.Success(response))
    }

    // May i use mapper in here?
    private fun mapToDatabaseObject(response: List<DevByteResponse>): List<DevByteEntity> {
        return response.map {
            DevByteEntity(
                0,
                it.title,
                it.description,
                it.videoUrl,
                it.updatedDate,
                it.thumbnailUrl
            )
        }
    }
}
