package com.sannedak.devbytes.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.sannedak.devbytes.data.model.DevByteEntity
import com.sannedak.devbytes.data.repository.DevBytesDao

@Database(entities = [DevByteEntity::class], version = 1)
abstract class DevBytesDatabase : RoomDatabase() {

    abstract fun devBytesDao(): DevBytesDao

    companion object {

        @Volatile
        private var INSTANCE: DevBytesDatabase? = null

        fun getDatabase(context: Context): DevBytesDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    DevBytesDatabase::class.java,
                    "dev_bytes_database"
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}