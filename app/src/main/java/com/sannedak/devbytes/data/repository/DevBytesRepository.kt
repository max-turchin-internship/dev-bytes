package com.sannedak.devbytes.data.repository

import com.sannedak.devbytes.data.DataState
import kotlinx.coroutines.flow.flow

interface DevBytesRepository {

    suspend fun getDevBytes() = flow<DataState<Any>> { }
}
