package com.sannedak.devbytes.data.repository

import com.sannedak.devbytes.data.model.DevByteResponse
import retrofit2.http.GET

interface DevBytesUdacityRepository {

    @GET("devbytes.json")
    suspend fun getDevBytes(): List<DevByteResponse>
}
