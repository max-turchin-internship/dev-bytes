package com.sannedak.devbytes.data.repository

import com.sannedak.devbytes.data.DataState
import com.sannedak.devbytes.data.model.DevByteResponse
import kotlinx.coroutines.flow.flow

class DevBytesMockRepository {

    fun getDevBytes() = flow {
        emit(DataState.Downloading)
        val list = listOf(
            DevByteResponse(
                "What's new in Android Studio 4.1",
                "The Android Studio 4.1 release introduces a Database inspector, " +
                        "Dagger navigation support, a native memory profiler, improvements " +
                        "for Apply Changes, the ability to use the emulator directly inside " +
                        "Android Studio, and more!\n\nThe new IntelliJ 2020.1 update features a " +
                        "new Commit tool window, a new Interactively Rebase dialog, the ability " +
                        "to switch between branches directly from the bottom bar, Javadocs " +
                        "rendering within the editor, and more.\n\nLearn more about the updates " +
                        "→ https://goo.gle/34P6Ek7 \nApply Changes → https://goo.gle/33NioD5 " +
                        "\nMigrating to Material Components for Android → https://goo.gle/3hW4KCX " +
                        "\nDesign Tools Suite updates → https://goo.gle/3hQ55H6 \n\nAndroid Tool " +
                        "Time playlist → https://goo.gle/307zGbu    \nSubscribe to the Android " +
                        "Developers channel → https://goo.gle/AndroidDevs  \n\n\n#Featured " +
                        "#Develop #AndroidStudio",
                "https://www.youtube.com/watch?v=Yhbr6u7f3ME",
                "2020-10-12T16:45:02+00:00",
                "https://i2.ytimg.com/vi/Yhbr6u7f3ME/hqdefault.jpg"
            ),
            DevByteResponse(
                "Android Basics in Kotlin: Course trailer",
                "Android Basics in Kotlin is a free online course offered by Google " +
                        "that teaches you the fundamentals of how to build an Android app. " +
                        "No programming experience is required. With step-by-step instructions, " +
                        "you’ll learn the Kotlin programming language and how to use Android " +
                        "Studio to build a collection of apps! \n\nAndroid Basics in Kotlin " +
                        "course → https://g.co/android/basics  \n\nSubscribe to Android " +
                        "Developers → https://goo.gle/AndroidDevs \n\n#Featured #Kotlin #Developers",
                "https://www.youtube.com/watch?v=oSim9fBFy-E",
                "2020-07-16T12:30:04+00:00",
                "https://i4.ytimg.com/vi/oSim9fBFy-E/hqdefault.jpg"
            ),
            DevByteResponse(
                "Support for newer Java language APIs",
                "When you use Android Gradle Plugin 4.0.0 and newer, you can now use " +
                        "several hundred APIs from newer OpenJDK versions and your app will " +
                        "work on any Android device? Some of these newer Java APIs in Android " +
                        "11 are supported through backporting while other APIs are also " +
                        "available via desugaring on older devices where the Android platform " +
                        "does not have the APIs on the runtime.\n\nResources:\nCheck out the " +
                        "11 Weeks of Android website → https://goo.gle/30FDT8S\nFull list of " +
                        "supported Java 8+ APIs → https://goo.gle/3enaGD5 \n\nRelated " +
                        "Playlists:\nLanguages - 11 Weeks of Android playlist " +
                        "→ https://goo.gle/languages-11weeks  \n11 Weeks of Android " +
                        "playlist → https://goo.gle/android-11weeks \n\nSubscribe to Android " +
                        "Developers → https://goo.gle/AndroidDevs \n\nSpeaker:\nMurat " +
                        "Yener\n\n#featured #android11 #11WeeksOfAndroid",
                "https://www.youtube.com/watch?v=heCvGfOGH0s",
                "2020-07-14T13:00:18+00:00",
                "https://i1.ytimg.com/vi/heCvGfOGH0s/hqdefault.jpg"
            )
        )
        emit(DataState.Success(list))
    }
}