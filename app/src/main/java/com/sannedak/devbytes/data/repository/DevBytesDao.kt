package com.sannedak.devbytes.data.repository

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.sannedak.devbytes.data.model.DevByteEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface DevBytesDao {

    @Query("SELECT * FROM dev_bytes")
    fun getDevBytes(): Flow<DevByteEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(devBytes: List<DevByteEntity>)
}
