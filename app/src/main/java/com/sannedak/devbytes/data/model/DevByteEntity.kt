package com.sannedak.devbytes.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "dev_bytes")
data class DevByteEntity(

    @PrimaryKey(autoGenerate = true) val id: Int,
    val title: String,
    val description: String,
    @ColumnInfo(name = "video_url") val videoUrl: String,
    @ColumnInfo(name = "updated_date") val updatedDate: String,
    @ColumnInfo(name = "thumbnail_url") val thumbnailUrl: String
)
