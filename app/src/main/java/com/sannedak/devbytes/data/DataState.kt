package com.sannedak.devbytes.data

import com.sannedak.devbytes.data.model.DevByteResponse

sealed class DataState<T> {

    object Downloading : DataState<Any>()
    object Error : DataState<Any>()
    data class Success<T>(val data: List<DevByteResponse>) : DataState<T>()
}
