package com.sannedak.devbytes.data.model

data class DevByteResponse(

    val title: String,
    val description: String,
    val videoUrl: String,
    val updatedDate: String,
    val thumbnailUrl: String
)
