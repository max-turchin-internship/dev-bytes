package com.sannedak.devbytes.data.network

import com.sannedak.devbytes.data.model.DevByteResponse
import com.squareup.moshi.FromJson
import com.squareup.moshi.JsonReader

class DevByteJsonAdapter {

    @FromJson
    fun fromJson(jsonRader: JsonReader): List<DevByteResponse> {
        val devByteList = mutableListOf<DevByteResponse>()
        jsonRader.beginObject()
        jsonRader.nextName()
        jsonRader.beginArray()
        while (jsonRader.hasNext()) {
            jsonRader.beginObject()
            var title = ""
            var description = ""
            var url = ""
            var updated = ""
            var thumbnail = ""

            while (jsonRader.hasNext()) {
                when (jsonRader.nextName()) {
                    "title" -> title = jsonRader.nextString()
                    "description" -> description = jsonRader.nextString()
                    "url" -> url = jsonRader.nextString()
                    "updated" -> updated = jsonRader.nextString()
                    "thumbnail" -> thumbnail = jsonRader.nextString()
                }
            }
            jsonRader.endObject()

            val devByte = DevByteResponse(title, description, url, updated, thumbnail)
            devByteList.add(devByte)
        }
        jsonRader.endArray()
        jsonRader.endObject()
        return devByteList
    }
}
