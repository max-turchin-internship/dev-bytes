package com.sannedak.devbytes.worker

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.sannedak.devbytes.DevBytesApplication

class RefreshDataWorker(appContext: Context, params: WorkerParameters) :
    CoroutineWorker(appContext, params) {

    companion object {
        const val WORK_NAME = "RefreshDataWorker"
    }

    override suspend fun doWork(): Result {
        val repository = DevBytesApplication().repository
        return try {
            repository.saveDevBytesToDatabase()
            Result.success()
        } catch (e: Exception) {
            Result.retry()
        }
    }
}