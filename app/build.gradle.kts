plugins {
    id(BuildOptions.androidApplication)
    id(BuildOptions.kotlinAndroid)
    id(BuildOptions.kotlinKapt)
    id(BuildOptions.navigationSafeArgs)
}

android {
    compileSdkVersion(AndroidSdk.compile)

    defaultConfig {
        applicationId = Config.appId
        minSdkVersion(AndroidSdk.minimal)
        targetSdkVersion(AndroidSdk.minimal)
        versionCode(Config.code)
        versionName(Config.name)

        testInstrumentationRunner = Config.testRunner
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {

    // Kotlin
    implementation(Kotlin.stdlib)
    implementation(KotlinX.coroutines.core)
    //implementation(KotlinX.coroutines.android)

    // WorkManager
    implementation(AndroidX.work.runtimeKtx)

    // AndroidX
    implementation(AndroidX.core)
    implementation(AndroidX.appCompat)
    implementation(AndroidX.activityKtx)
    implementation(AndroidX.fragmentKtx)

    // Lifecycle
    implementation(AndroidX.lifecycle.viewModelKtx)
    implementation(AndroidX.lifecycle.liveDataKtx)
    implementation(AndroidX.lifecycle.viewModelSavedState)

    // Navigation
    implementation(AndroidX.navigation.uiKtx)
    implementation(AndroidX.navigation.fragmentKtx)

    // UI
    implementation(Google.android.material)
    implementation(AndroidX.constraintLayout)

    // Room
    implementation(AndroidX.room.runtime)
    implementation(AndroidX.room.ktx)
    kapt(AndroidX.room.compiler)

    // Retrofit
    implementation(Square.retrofit2.retrofit)
    implementation(Square.retrofit2.converter.moshi)
    implementation(Square.moshi)
    implementation(Square.moshi.kotlinReflect)

    // Glide
    implementation(Libs.glide)
    annotationProcessor(Libs.glideCompiler)

    // Retrofit
    implementation(Square.retrofit2.retrofit)
    implementation(Square.retrofit2.converter.moshi)
    implementation(Square.moshi)
    implementation(Square.moshi.kotlinReflect)

    // Test
    testImplementation(Testing.junit)
    androidTestImplementation(AndroidX.test.ext.junitKtx)
    androidTestImplementation(AndroidX.test.espresso.core)
}
