object Libs {

    object Versions {
        const val glideVersion = "4.11.0"
    }

    const val glide = "com.github.bumptech.glide:glide:${Versions.glideVersion}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${Versions.glideVersion}"
}
